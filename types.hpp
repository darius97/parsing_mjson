#ifndef _TYPES_
#define _TYPES_


#include <cstdint>

using i32 = int32_t;
using u32 = uint32_t;
#endif 