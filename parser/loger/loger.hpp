#ifndef _LOGER_
#define _LOGER_


#include "../macro.hpp"
#include "../types.hpp"
#include "../file/file.hpp"
#include <array>
#include <string>

#define   LOG_TRACE(message) loger::Loger::get_instance().log(loger::Level::TRACE_LEVEL, message, __LINE__, __FILE__)  
#define   LOG_DEBUG(message) loger::Loger::get_instance().log(loger::Level::DEBUG_LEVEL, message, __LINE__, __FILE__) 
#define LOG_WARNING(message) loger::Loger::get_instance().log(loger::Level::WARNING_LEVEL, message, __LINE__, __FILE__)
#define   LOG_ERROR(message) loger::Loger::get_instance().log(loger::Level::ERROR_LEVEL, message, __LINE__, __FILE__)
#define   LOG_FATAL(message) loger::Loger::get_instance().log(loger::Level::FATAL_LEVEL, message, __LINE__, __FILE__)

namespace loger {

    static constexpr std::size_t LEVEL_COUNT = 5;

    enum class Level {
        TRACE_LEVEL = 0,
        DEBUG_LEVEL = 1,
        WARNING_LEVEL = 2, 
        ERROR_LEVEL = 3, 
        FATAL_LEVEL = 4
    };

    class Loger {
    public:
        ~Loger() = default; 

//        NO_COPY_NO_MOVE(Loger)

        static void log(
            Level level, const std::string &message, 
            const i32 line, const std::string &file
            );
        static Loger& get_instance() { return minstance; }

    private:
        Loger();

        static File mfile;
        static Loger minstance;

        static std::array<std::string, LEVEL_COUNT> mlevel_str; 
    }; 

}

#endif