#include "file.hpp"
#include "../types.hpp"
#include <string>
#include <cassert>
#include <stdio.h>


File::File(const std::string &path_file) 
    : mpath_file{ path_file } {
    mfile = fopen(mpath_file.c_str(), "r+");
    assert(mfile != NULL); // TODO: print loger
//   mbuffer.reserve(512);
}
File::~File() {
//   flush();
   fclose(mfile);
}

void File::read_fully(std::string &buffer) {
    i32 input;        
    while ((input = fgetc(mfile)) != EOF) {
        buffer += static_cast<char>(input);
    }   
}

void File::write(const std::string &output) {
    fprintf(mfile, "%s", output.c_str());
    fflush(mfile);
/*    if (output.size() + mbuf_offset > mbuffer.size()) {
        flush();
    }
    mbuffer.assign(output.begin(), output.end());
    mbuf_offset += output.size();*/
}
/*void File::flush() {
    if (mbuf_offset != 0) {
        fprintf(mfile, "%s", mbuffer.c_str());
        mbuf_offset = 0;
    }
}*/