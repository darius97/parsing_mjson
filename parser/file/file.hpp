#ifndef _FILE_
#define _FILE_


#include "../macro.hpp"
#include <string>


class File {
public:
    File(const std::string &path_file);
    ~File();
    
    NO_COPY_NO_MOVE(File)

    void read_fully(std::string &buffer);
    void write(const std::string &output);

private:
    std::string mpath_file;
//    std::size_t mbuf_offset;
//    std::string mbuffer;
    FILE* mfile;
    
    void flush();
};
#endif