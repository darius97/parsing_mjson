#ifndef _AST_
#define _AST_


#include "../../macro.hpp"
#include "../../types.hpp"
#include "symbl_table.hpp"
#include "units.hpp"
#include <tuple>
#include <memory>
namespace ast {

    struct NNode {
        NNode(
        NNode *parent_,
        const Units value_, const i32 id_,  const bool is_end_
        ) : parent{ parent_ }, l_child{ nullptr }, r_child{ nullptr },
          value{ value_ }, id{ id_ }, is_end{ is_end_ } {}
        
        NNode *parent;
        std::unique_ptr<NNode> l_child, r_child;
        Units value;
        i32 id; 
        bool is_end;       
    };
    
    class Ast {
    public:
        Ast();
        ~Ast();

        NO_COPY_NO_MOVE(Ast)

        void add_node(const Units value, const i32 id = -1);
        
        std::tuple<bool, bool> get_bool(const std::string &identifier_name) const;

        void set_symbl_table(const SymblTable *st) {
            msymbl_table = st;
        }

    private:
        std::unique_ptr<NNode> mroot;
        NNode *mlast;
        const SymblTable *msymbl_table;

        std::unique_ptr<NNode> create_node(
            const Units value, NNode *mparent, const i32 id = -1
            ); 

        static bool is_terminal(const Units unit);
        static bool is_ignore(const Units unit);

        static bool is_2inlevel(const Units units);

        void add_2inlevel(const Units units, const i32 id);
        void add_1inlevel(const Units units, const i32 id);

        void router();
        void return_to_last_left_notfull_branch();

        NNode* dfs_search(const i32 id) const;
        NNode* sub_search(NNode *node_, const i32 id) const;
    };

}
#endif 