#include "symbl_table.hpp"
#include "../../types.hpp"
#include <map>
#include <string>
     

std::string SymblTable::get_token_value(const i32 id) const {
    auto search = msymbol_table.find(id);
    if (search == msymbol_table.end()) {
        return "";
    }
    return search->second.first;
}

i32 SymblTable::get_token_id(const std::string &identifier_name) const {
    // TODO: make unordered_map<std::string&, i32&> - (value, id)
    // it will be work O(1), current implementation work O(n) + cmp str
    
    for (auto iterator : msymbol_table) {
        if (iterator.second.first == identifier_name) {
            return iterator.first;
        }
    }
    return -2; // not found
}

void SymblTable::insert(
    const i32 id, const std::string &value, const NumberType units
    ) { 
    
    msymbol_table.insert({ id, { value, units } });
}






