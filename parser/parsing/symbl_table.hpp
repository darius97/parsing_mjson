#ifndef _SYMBL_TABLE_
#define _SYMBL_TABLE_


#include "../../macro.hpp"
#include "../../types.hpp"
#include "units.hpp"
#include <map>
#include <string>

class SymblTable {
public:
    std::string get_token_value(const i32 id) const;
    i32 get_token_id(const std::string &identifier_name) const;
    
    void insert(const i32 id, const std::string &value, const NumberType unit); 

private:    
    // id, <value, type>
    typedef std::map<unsigned, std::pair<std::string, NumberType>> SymbolTable;
    SymbolTable msymbol_table;       
};

#endif




