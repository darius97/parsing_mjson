#include "parser.hpp"
#include "ast.hpp"
#include "../../loger/loger.hpp"
#include <string>
#include <stdio.h>
#include <cassert>
#include <tuple>

int main() {
    parser::Parser parsing;
    ast::Ast ast_file0;
//    assert(parsing.parsing("../test_config/error_array.ini") == false);
//    assert(parsing.parsing("../test_config/array_1.ini") == true);
//    assert(parsing.parsing("../test_config/error_table.ini") == false);
    
//    assert(parsing.parsing("../test_config/array_n.ini") == true);
//    assert(parsing.parsing("../test_config/nested_table.ini", ast_file0) == true);    
//    assert(parsing.parsing("../test_config/empty_table.ini") == true);
    assert(parsing.parsing("../test_config/target.ini", ast_file0) == true);
    bool is_ok, value;
    std::tie(is_ok, value) = ast_file0.get_bool("zeros");
    is_ok ? printf("%d\n", value) : printf("not found\n");
    return 0;





} 