#include "parser.hpp"
#include "ast.hpp"
#include "../../loger/loger.hpp"
#include <stdio.h>
#include <string>
#include <map>
#include <vector>
#include <stack>
#include <cassert>

namespace parser {

    std::map<std::pair<Units, Units>, unsigned> fms_parsing = {
        { { Units::CONFIGR,  Units::START_CURLY_BRACE  }, 0  }, 
        { { Units::CONFIGR,  Units::EOF_               }, 15 },
        
        { { Units::TABLE,    Units::START_CURLY_BRACE  }, 1  },
        
        { { Units::MEMLIST,  Units::EOF_               }, 15 }, 
        { { Units::MEMLIST,  Units::END_CURLY_BRACE    }, 15 },
        { { Units::MEMLIST,  Units::IDENTIFIER         }, 2  }, 

        { { Units::MEMLIST_, Units::SEMICOLON          }, 3  },

        { { Units::X,        Units::EOF_               }, 15 },
        { { Units::X,        Units::END_CURLY_BRACE    }, 15 },
        { { Units::X,        Units::SEMICOLON          }, 4  },

        { { Units::MEMBR,    Units::END_CURLY_BRACE    }, 15 },
        { { Units::MEMBR,    Units::IDENTIFIER         }, 5  },
        { { Units::MEMBR,    Units::EOF_               }, 15 },
 
        { { Units::VALUE,    Units::START_CURLY_BRACE  }, 1  },
        { { Units::VALUE,    Units::START_SQUARE_BRACE }, 14 },
        { { Units::VALUE,    Units::STRING             }, 8  },
        { { Units::VALUE,    Units::NULL_              }, 6  },
        { { Units::VALUE,    Units::BOOL               }, 7  },
        { { Units::VALUE,    Units::NUMBER             }, 9  },

        { { Units::ARRAY,    Units::START_SQUARE_BRACE }, 10 },

        { { Units::ELIST,    Units::START_CURLY_BRACE  }, 11 },
        { { Units::ELIST,    Units::START_SQUARE_BRACE }, 11 },
        { { Units::ELIST,    Units::STRING             }, 11 },
        { { Units::ELIST,    Units::NULL_              }, 11 },
        { { Units::ELIST,    Units::BOOL               }, 11 }, 
        { { Units::ELIST,    Units::NUMBER             }, 11 },
        { { Units::ELIST,    Units::EOF_               }, 15 },
        { { Units::ELIST,    Units::END_SQUARE_BRACE   }, 15 },

        { { Units::ELIST_,   Units::COMMA              }, 12 },

        { { Units::Y,        Units::COMMA              }, 13 },
        { { Units::Y,        Units::END_SQUARE_BRACE   }, 15 },
        { { Units::Y,        Units::EOF_               }, 15 }         
    };

    // grammar's rule are inverse for correct adding to stack_parsing
    std::map<unsigned, std::vector<Units>> grammar_table = { 
        { 0,  { Units::TABLE                                                        } },
        { 1,  { Units::END_CURLY_BRACE,   Units::MEMLIST, Units::START_CURLY_BRACE  } },
        { 2,  { Units::X,                 Units::MEMBR                              } },
        { 3,  { Units::X,                 Units::MEMBR,   Units::SEMICOLON          } },
        { 4,  { Units::MEMLIST_                                                     } },
        { 5,  { Units::VALUE,             Units::EQUAL,   Units::IDENTIFIER         } },
        { 6,  { Units::NULL_                                                        } },
        { 7,  { Units::BOOL                                                         } },
        { 8,  { Units::STRING                                                       } },
        { 9,  { Units::NUMBER                                                       } },
        { 10, { Units::END_SQUARE_BRACE, Units::ELIST,    Units::START_SQUARE_BRACE } },
        { 11, { Units::Y,                Units::VALUE                               } },
        { 12, { Units::Y,                Units::VALUE,    Units::COMMA              } },
        { 13, { Units::ELIST_                                                       } },
        { 14, { Units::ARRAY,                                                       } },
        { 15, {                                                                     } }
    };
    
    
    loger::Loger Parser::mloger{ loger::Loger::get_instance() };

    bool Parser::parsing(const std::string &path_file, ast::Ast &ast) {
        ast.set_symbl_table(&msymbl_table);
        
        Lexer lexer{ path_file };
        lexer.lexer(mtokens, msymbl_table); 

        std::stack<Units> stack_parsing;
        stack_parsing.push(Units::CONFIGR);
        i32 id = -1;
        for (auto token : mtokens) {
            for (bool is_next = false; is_next != true; ) {
                if (stack_parsing.top() == token.first) {
                    stack_parsing.pop();
                    is_next = true;
                    continue;
                }
            
                auto search = fms_parsing.find({ stack_parsing.top(), token.first });
                if (search == fms_parsing.end()) {
                    LOG_ERROR("error in parsing"); // TODO: make more informating message(place, what)
                    return false;
                }

                stack_parsing.pop();             
                for (auto unit : grammar_table[search->second]) {
                    stack_parsing.push(unit);
                    printf("%d\t", static_cast<int>(unit));
                    (is_terminal(unit)) ? id = token.second : id = -1; 
                    ast.add_node(unit, id);
                }
                printf("\n");
            }
        }
//        mast_tree.tree_print();
        return stack_parsing.empty();
    } 

    bool Parser::is_terminal(const Units unit) {
        return unit == Units::IDENTIFIER
            || unit == Units::STRING
            || unit == Units::NUMBER
            || unit == Units::BOOL
            || unit == Units::NULL_; 
    }




}