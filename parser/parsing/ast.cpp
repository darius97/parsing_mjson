#include "ast.hpp"
#include "units.hpp"
#include <memory>
#include <utility>
#include <string>
#include <tuple>
#include <stack>

namespace ast {

    Ast::Ast() 
    : mroot{ create_node(Units::CONFIGR, nullptr) }, mlast{ mroot.get() }  {}
    
    Ast::~Ast() {} 
    
    void Ast::add_node(const Units units, const i32 id) {
        if (is_ignore(units)) { 
            return;         
        }
        else if (is_2inlevel(units)) {
            add_2inlevel(units, id);
        } else {
            add_1inlevel(units, id);
        }        
    }
    
    bool Ast::is_2inlevel(const Units units) {
        return units == Units::X || units == Units::MEMBR 
            || units == Units::Y || units == Units::VALUE
            || units == Units::IDENTIFIER;
    }
    
    void Ast::add_2inlevel(const Units units, const i32 id) {
        static i32 count = 0;
        static constexpr i32 COUNT_UNIT_PER_LEVEL = 2;
        static Units level[COUNT_UNIT_PER_LEVEL];
        level[count] = units;
        ++count;

        if (count == COUNT_UNIT_PER_LEVEL) {
            mlast->l_child = std::move((create_node(level[0], mlast, id)));
            mlast->r_child = std::move((create_node(level[1], mlast, id)));
            router();
            
            count = 0;
        }
    }
    
    void Ast::add_1inlevel(const Units units, const i32 id) {
        mlast->r_child = std::move((create_node(units, mlast, id)));
        router();
    }
    
    void Ast::router() {
        if(mlast->value == Units::MEMBR
            && mlast->r_child->is_end == true) {
            mlast->is_end = true;
        }
            
        if (mlast->r_child->is_end != true) {
            mlast = mlast->r_child.get();
        } else if (
            (mlast->r_child->is_end == true && mlast->l_child == nullptr)
            || (mlast->r_child->is_end == true && mlast->l_child->is_end == true)
            ) {
                return_to_last_left_notfull_branch();        
        } else {
            mlast = mlast->l_child.get();
        }
    }  
    
    void Ast::return_to_last_left_notfull_branch() {
        while (mlast->l_child == nullptr || mlast->l_child->is_end == true) {
            mlast->is_end = true;
            mlast = mlast->parent; 
        }   
        mlast = mlast->l_child.get();
    }
     
    std::unique_ptr<NNode> Ast::create_node(
        const Units value, NNode *mparent, const i32 id
        ) {
        bool is_end = false;
        if (is_terminal(value)) {
            is_end = true;
        }       
        std::unique_ptr<NNode> nw_node(new NNode(mparent, value, id, is_end));
/*        nw_node->l_child = nullptr;
        nw_node->r_child = nullptr;
        nw_node->parent = mparent;
        nw_node->value = value;
        nw_node->id = id;
        is_terminal(value) ? nw_node->is_end = true : nw_node->is_end = false;
        */
        return nw_node;            
    }
       
    bool Ast::is_terminal(const Units unit){
        return unit == Units::IDENTIFIER
            || unit == Units::STRING
            || unit == Units::NUMBER
            || unit == Units::BOOL
            || unit == Units::NULL_; 
    }
    
    bool Ast::is_ignore(const Units unit) { 
        return unit == Units::START_CURLY_BRACE 
            || unit == Units::END_CURLY_BRACE
            || unit == Units::START_SQUARE_BRACE 
            || unit == Units::END_SQUARE_BRACE
            || unit == Units::EQUAL 
            || unit == Units::COMMA
            || unit == Units::SEMICOLON;
    } 

    std::tuple<bool, bool> Ast::get_bool(const std::string &identifier_name) const {
        i32 id = msymbl_table->get_token_id(identifier_name);
        if (id == -2) {
            return std::make_tuple<bool, bool>(false, false);
        }
        NNode *searched = dfs_search(id);  
        if (searched == nullptr) {   
            return std::make_tuple<bool, bool>(false, false);  
        }                 
        searched = searched->parent;
        if (searched->l_child->value == Units::VALUE) {
            searched = searched->l_child.get();
            if (searched->r_child->value == Units::BOOL) {
                bool value = msymbl_table->get_token_value(searched->r_child->id) == "true";
                return std::make_tuple<bool, bool>(true, std::move(value));
            }
        }
        return std::make_tuple<bool, bool>(false, false);
    }
    
    NNode* Ast::dfs_search(const i32 id) const {
        std::stack<NNode*> stack;
        stack.push(mroot.get());

        for (; !stack.empty(); ) {
            if (stack.top()->id == id) {
                return stack.top(); 
            }
            
            NNode *l_child = stack.top()->l_child.get();
            NNode *r_child = stack.top()->r_child.get();

            stack.pop();
            
            if (l_child != nullptr) {
                stack.push(l_child);
            }
            if (r_child != nullptr) {
                stack.push(r_child);
            }
        }
        return nullptr;
    }
    
}






