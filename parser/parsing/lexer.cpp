#include "parser.hpp"
#include "symbl_table.hpp"
#include "../../loger/loger.hpp"
#include <stack>
#include <cassert>
#include <string>


namespace parser {

    loger::Loger Parser::Lexer::mloger{ loger::Loger::get_instance() };
    
    Parser::Lexer::Lexer(const std::string &path_file) : mfile{ path_file } {}

    void Parser::Lexer::lexer(TokensTable &token, SymblTable &symbol_table) {
        mfile.read_fully(minput);

        assert(is_bracket_correct());
        tokenizer(token, symbol_table);
        minput.clear();
    }

    bool Parser::Lexer::is_bracket_correct() const {
        std::stack<char> brackets;
        for (auto iterator = minput.begin(); iterator != minput.end(); ++iterator) {
            if (*iterator == '[' || *iterator == '{') {
                brackets.push(*iterator);
            }
            else if ((*iterator == ']' && brackets.size() > 0 && brackets.top() == '[')
            || (*iterator == '}' && brackets.size() > 0 && brackets.top() == '{')) {
                brackets.pop();
            }
            else if (*iterator == '{' || *iterator == '}' 
                || *iterator == '[' || *iterator == ']') {
                LOG_ERROR("incorrect bracket");
                return false;
            }
        }
        if (!brackets.empty()) {
            LOG_ERROR("incorrect bracket");
        }
        return brackets.empty();
    }

    void Parser::Lexer::tokenizer(
        TokensTable &tokens, SymblTable &symbol_table
        ) {
        std::string token_value;
        i32 tokens_id = 1;

        unsigned line_counter = 1;
        auto minput_end = minput.end();
        for (auto iterator = minput.begin(); iterator != minput_end; ++iterator) {
            if (isspace(*iterator)) {
                if (*iterator == '\n' || *iterator == '\r') { //    GNU/Linux — \n; (Mac) — \r; Windows — \r\n.
                    ++line_counter;
                }
                continue;
            }
            else if (*iterator == ';' || *iterator == ',' ||
                *iterator == '{' || *iterator == '}' || // NOTE: profile 
                *iterator == '[' || *iterator == ']' ||
                *iterator == '=') {
                process_single_unit(*iterator, tokens);
            }
            // STRING
            else if (*iterator == '"') {  
                ++iterator;
                for (; *iterator != '"'; ++iterator) {
                    token_value += *iterator;
                }
                tokens.push_back({ Units::STRING, tokens_id });
                symbol_table.insert(tokens_id, token_value, NumberType::NOT_NUMBER);
                ++tokens_id;
                token_value.clear();
            }
            // IDENTIFIER // BOOL, NULL_
            else if (isalpha(*iterator) || (*iterator) == '_') {
                Units units = Units::IDENTIFIER;

                for (; *iterator != ' ' && *iterator != '=' && *iterator != ';'; ++iterator) {
            //  for (; isdigit(*iterator) || isalpha(*iterator) || *iterator == '_'; ++iterator) {
                    token_value += *iterator;
                }
                --iterator; // for catch '=' and ';'
                if (token_value == "true" || token_value == "false") {
                    units = Units::BOOL;
                }
                else if (token_value == "null") {
                    units = Units::NULL_;
                }
                tokens.push_back({ units, tokens_id });
                symbol_table.insert(tokens_id, token_value, NumberType::NOT_NUMBER);
                ++tokens_id;
                token_value.clear();
            }
            // NUMBER
            else if (isdigit(*iterator) || (*iterator == '-')) { 
                token_value += *iterator;
                ++iterator;

                bool is_float_point = false;
                for (;isdigit(*iterator) || (*iterator) == '.'; ++iterator) {
                    token_value += *iterator;
                    if (*iterator == '.') {
                        is_float_point = true;
                    }
                } 
                
                tokens.push_back({ Units::NUMBER, tokens_id });

                std::string number_type;
                while (*iterator != ' ' && *iterator != ',' && *iterator != ';'
                    && *iterator != ']' && *iterator != '}'
                    ) {
                    number_type += *iterator;
                    ++iterator;
                }
                --iterator; // for proccess next symbol after number
                
                NumberType nt = define_type(number_type, is_float_point);
                symbol_table.insert(tokens_id, token_value, nt);
                ++tokens_id;
                token_value.clear();
            }
            else {
                assert(true == false);
                LOG_ERROR("incorrect symbol");
            }
        }
    } 

    NumberType Parser::Lexer::define_type(
        const std::string &number_type, bool is_float_point
        ) {
        NumberType nt;
        if (number_type.empty()) { // default double and i32
            if (is_float_point) {
                nt = NumberType::DOUBLE;
            }
            else {
                nt = NumberType::I32;
            }
        }
        else {
            if (number_type == "i8") {
                nt = NumberType::I8;
            }
            else if (number_type == "i16") {
                nt = NumberType::I16;
            }
            else if (number_type == "i32") {
                nt = NumberType::I32;
            }
            else if (number_type == "i64") {
                nt = NumberType::I64;
            }
            else if (number_type == "u8") {
                nt = NumberType::U8;
            }
            else if (number_type == "u16") {
                nt = NumberType::U16;
            }
            else if (number_type == "u32") {
                nt = NumberType::U32;
            }
            else if (number_type == "u64") {
                nt = NumberType::U64;
            }
            else if (number_type == "f") {
                nt = NumberType::FLOAT;
            }
            else if (number_type == "d") {
                nt = NumberType::DOUBLE;
            }
            else {
                assert(true == false);
                nt = NumberType::ERROR;
                // error
            }
        }
        return nt;
    }
    void Parser::Lexer::process_single_unit(const char unit, TokensTable &tokens) {
        Units units;
        switch (unit) {
            case ',': 
                units = Units::COMMA;
                break;
            case ';':
                units = Units::SEMICOLON;
                break;
            case '{':
                units = Units::START_CURLY_BRACE;
                break;
            case '}':
                units = Units::END_CURLY_BRACE;
                break;
            case '[':
                units = Units::START_SQUARE_BRACE;
                break;
            case ']':
                units = Units::END_SQUARE_BRACE;
                break;
            case '=':
                units = Units::EQUAL;
                break;
            default:
                units = Units::UNKNOWN;
                break;
        }
        tokens.push_back({ units, 0 });
    }  
}




