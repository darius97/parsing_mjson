#ifndef _PARSER_
#define _PARSER_


#include <string>
#include <map>
#include <stack>
#include <vector>
#include "../../loger/loger.hpp"
#include "../../file/file.hpp"
#include "../../macro.hpp"
#include "ast.hpp"
#include "units.hpp"
#include "symbl_table.hpp"


namespace parser {   
   
    // TokenUnits, id
    typedef std::vector<std::pair<Units, unsigned>> TokensTable;
  
    class Parser {
    public:
        Parser() = default;
        ~Parser() = default; 

        NO_COPY_NO_MOVE(Parser)

        bool parsing(const std::string &path_file, ast::Ast &ast);

        class Lexer {
        public: 
            explicit Lexer(const std::string &path_file);
            ~Lexer() = default;

            NO_COPY_NO_MOVE(Lexer)

            void lexer(TokensTable &token, SymblTable &symbol_table);  

        private:
            File mfile;
            std::string minput;

            static loger::Loger mloger;

            bool is_bracket_correct() const;
            void tokenizer(TokensTable &token, SymblTable &symbol_table); 
            
            NumberType define_type(
                const std::string &number_type, bool is_float_point
                );            
            void process_single_unit(const char unit, TokensTable &tokens);
        };

        static loger::Loger mloger;
        TokensTable mtokens;
        SymblTable msymbl_table;
        static bool is_terminal(const Units unit);
    };

}
#endif




