#ifndef _UNITS_
#define _UNITS_


enum class Units {
        CONFIGR,
        TABLE,
        MEMLIST,
        MEMLIST_,
        X,
        MEMBR,
        VALUE,
        ARRAY,
        ELIST,
        ELIST_,
        Y,
       
        START_CURLY_BRACE, END_CURLY_BRACE,
        START_SQUARE_BRACE, END_SQUARE_BRACE,
        EQUAL,
        COMMA, 
        SEMICOLON,
        IDENTIFIER, STRING, NUMBER, BOOL, NULL_,
        UNKNOWN,
        EOF_,
};
enum class NumberType {
        I8, I16, I32, I64, // I32 default
        U8, U16, U32, U64,
        FLOAT,
        DOUBLE, // default float_point
        NOT_NUMBER, // for STRING BOOL NULL IDENTIFIER 
        ERROR // for unknown
};




#endif 