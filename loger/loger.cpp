#include "loger.hpp"
#include "../types.hpp"

#include <string>


namespace loger {

    Loger::Loger() {}

    void Loger::log(
        Level level, const std::string &message, 
        const i32 line, const std::string &file
        ) {
        std::string format_message = 
            file + ":" + std::to_string(line) + ": " +
            "[" + mlevel_str[static_cast<i32>(level)] + "]: " + message + "\n";
            
        mfile.write(format_message);
    }

    std::array<std::string, LEVEL_COUNT> Loger::mlevel_str{
            "TRACE",
            "DEBUG",
            "WARNING", 
            "ERROR_LEVEL", 
            "FATAL_LEVEL"
        }; 
         
    File Loger::mfile{ "/home/seagull/projects/linux_c/__/parser/loger.ini" };
    Loger Loger::minstance;
}